package com.oleksandr.DAO;

import com.oleksandr.model.PK_WorksOn;
import com.oleksandr.model.WorksOnEntity;

public interface WorksOnDAO extends GeneralDAO<WorksOnEntity, PK_WorksOn> {
}
