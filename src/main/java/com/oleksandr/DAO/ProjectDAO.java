package com.oleksandr.DAO;

import com.oleksandr.model.ProjectEntity;

public interface ProjectDAO extends GeneralDAO<ProjectEntity, String> {
}
