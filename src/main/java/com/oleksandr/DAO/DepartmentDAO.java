package com.oleksandr.DAO;

import com.oleksandr.model.DepartmentEntity;

import java.sql.SQLException;

public interface DepartmentDAO extends GeneralDAO<DepartmentEntity, String> {
}


